//version inicial
const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var requestjson = require('request-json');

const port = process.env.PORT || 3000;
console.log('Todo list RESTful API server started on: ' + port);

//require('./database');
var globals = require('./util/global');
const apiKey = globals.apiKey;
const urlUsu = globals.urlUsuarios;
const urlPro = globals.urlProductos;
const urlMov = globals.urlMovimientos;
const urlCtaE = globals.urlCtaExteriores;
const urlOfer = globals.urlOfertas;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Request-With, Content-Type, Accept");
  next();
});

var path = require('path');

const bcrypt = require('bcrypt');

const saltRounds = 10;

app.listen(port);

app.get('/', function (req,res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/clientes/:idcliente', function (req, res) {
  res.send("Aqui tiene al cliente número " + req.params.idcliente);
});

app.get('/v1/movimientos', function(req, res){
  var movimientosJSON = require('./movimientosv1.json');
  //res.sendFile(path.join(__dirname, movimientosJSON));
  res.send(movimientosJSON);
});

app.post('/', function (req,res) {
  res.send("Hemos recibido su peticion post");
});

app.put('/', function (req,res) {
  res.send("Hemos recibido su peticion put cambiada");
});

app.delete('/', function(req,res) {
  res.send("Hemos recibido su peticion delete");
});

//register: Registrar un nuevo usuario en DB
app.post('/register', function (req, res){
  var userMLab = requestjson.createClient(urlUsu + '?' + apiKey);
  bcrypt.hash(req.body.password, saltRounds, function (err, hash)
  {
      userMLab.post('', { tipoDoc: req.body.tipoDoc, documento: req.body.documento, email: req.body.email, password: hash }, function(err, resM, body){
        if(err){
          console.log("Se encontró un error en POST register: " + body);
        }else{
          console.log("¡Usuario registrado!");
          res.send(body);
        }
      })
  });
});

app.get('/login/:tipodoc/:usuario/:password',  function(req, res) {
    var urlLoginUserMLab = requestjson.createClient(urlUsu + '?' + apiKey + '={"tipoDoc": "' + req.params.tipodoc + '", "documento":"' + req.params.usuario + '"}');

    urlLoginUserMLab.get('', function(err, resM, result){
      if(err){
        console.log("Hubo error al consultar usuario.");
      }else{
        let response = result[0] ? result[0] : null;
        if (!response) {
          return res.send({code: 401, errorMessage: 'Error'})
        }
        pwd = response.password;

        bcrypt.compare(req.params.password, pwd, function(err, bres) {
            if (bres == true) {
                //console.log('Login satisfactorio para: '+ response.documento);
                res.status(200).send(response);
            } else {
                //console.log("Usuario o password errado.");
                let resbody ={"code" : 401 , "errorMessage" : "Usuario y/o contraseña invalidas"};
                res.status(401).send(resbody);
                bres.close();
            }
        });
      }
    });
});


app.get('/movimientos', function(req,res) {
  var moviMLab = requestjson.createClient(urlMov + '?'+ apiKey);
  moviMLab.get('', function(err, resM, body){
    if(err){
      console.log("Error en GET movimientos: " + err);
    }else{
      res.send(body);
    }
  })
});

app.get('/cuentasexterior', function(req,res) {
  var moviMLab = requestjson.createClient(urlCtaE + '?'+ apiKey);
  moviMLab.get('', function(err, resM, body){
    if(err){
      console.log("Error en GET cuentasexterior: " + err);
    }else{
      res.send(body);
    }
  })
});

app.get('/productos/:docum/:param', function(req, res) {
  //Si param=00 lista productos completos, Si param=01 lista productos para envío de dinero
  if(req.params.param=='00'){
      var urlProductosMLab = requestjson.createClient(urlPro + '?' + apiKey + '={"documento":"' + req.params.docum + '"}&s={"fecha": 1}');
      urlProductosMLab.get('', function(err, resM, result){
        if(err){
          console.log("Hubo error al consultar productos.");
        }else{
          let resbody = JSON.stringify(result);
          res.send(resbody);
        }
      });
  }else{
      var urlProductosMLab = requestjson.createClient(urlPro + '?' + apiKey + '={"documento":"' + req.params.docum + '","codtipo": { $in:["CA","CL"]} } }"'); //"&f={"cuenta": 1, "saldo": 1}');
      urlProductosMLab.get('', function(err, resM, result){
        if(err){
          console.log("Hubo error al listar productos para envío de dinero.");
        }else{
          let resbody = JSON.stringify(result);
          res.send(resbody);
        }
      });
  }
});

app.get('/movimientos/:cuenta/:tarjeta', function(req, res) {
    let urlConex = "";
    if(req.params.cuenta === 'NULO'){
      urlConex = urlMov + '?' + apiKey + '={"tarjeta":"' + req.params.tarjeta + '"}&s={"fecha": -1}'
    }else{
      urlConex = urlMov + '?' + apiKey + '={"cuenta":"' + req.params.cuenta + '"}&s={"fecha": -1}'
    }
    var urlMovimientosMLab = requestjson.createClient(urlConex);
    urlMovimientosMLab.get('', function(err, resM, result){
      if(err){
        console.log("Hubo error al consultar movimientos.");
      }else{
        let resbody = JSON.stringify(result);
        res.send(resbody);
      }
    });
});

app.get('/tipocambio', function(req, res){
  var tipocambioJSON = require('./tipocambio.json');
  res.send(tipocambioJSON);
});

//update: Actualizar saldo de cuenta
app.post('/actualizaSaldo/:idCuenta', function (req, res){
  var userMLab = requestjson.createClient(urlPro + '/' + req.params.idCuenta + '?' + apiKey);
  userMLab.put('', {"$set":{"saldo": req.body.saldo }}, function(err, resM, body){
    if(err){
      console.log("Se encontró un error al actualizar SALDO: " + body);
    }else{
      res.send(body);
    }
  })
});

//registrar: Registrar el envío de dinero "Movimiento"
app.post('/registrarEnvio', function (req, res){
  var userMLab = requestjson.createClient(urlMov + '?' + apiKey);
  userMLab.post('', {cuenta: req.body.cuenta, tipomovimiento:"debito", monto: req.body.monto, fecha: req.body.fecha, desccompania:req.body.desccompania, latitud: req.body.latitud, longitud:req.body.longitud}, function(err, resM, body){
    if(err){
      console.log("Se encontró un error al registrar envío: " + body);
    }else{
      res.send(body);
    }
  })
});

//Consultamos ofertas por cliente
app.get('/ofertas/:docum', function(req, res) {
  var urlOfertasMLab = requestjson.createClient(urlOfer + '?' + apiKey + '={"documento":"' + req.params.docum + '","activo": 1}&s={"tipo": 1}');
  urlOfertasMLab.get('', function(err, resM, result){
    if(err){
      console.log("Hubo error al consultar ofertas.");
    }else{
      let resbody = JSON.stringify(result);
      res.send(resbody);
    }
  });
});

//Actualizar 'activo' de oferta no aceptada
app.post('/bajaofertas/:idofer', function (req, res){
  var userMLab = requestjson.createClient(urlOfer + '/' + req.params.idofer + '?' + apiKey);
  userMLab.put('', {"$set":{"activo": 0 }}, function(err, resM, body){
    if(err){
      console.log("Se encontró un error al actualizar estado de oferta: " + body);
    }else{
      res.send(body);
    }
  })
});

//Actualizar 'activo = 2' (2=tomar oferta)
app.post('/tomarofertas/:idofer', function (req, res){
  var userMLab = requestjson.createClient(urlOfer + '/' + req.params.idofer + '?' + apiKey);
  userMLab.put('', {"$set":{"activo": 2 }}, function(err, resM, body){
    if(err){
      console.log("Se encontró un error al actualizar estado de oferta: " + body);
    }else{
      res.send(body);
    }
  })
});
